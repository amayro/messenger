from subprocess import Popen, CREATE_NEW_CONSOLE

pr_list = []
user_input = 's'

while user_input.lower() != 'q':

    user_input = input(f"Введите сколько запустить клиентов / Закрыть клиентов (x) / Выйти (q) ")

    try:
        count_clients = abs(int(user_input))
    except ValueError:
        count_clients = 0

    if count_clients:
        for _ in range(count_clients):
            pr_list.append(Popen("python main.py", creationflags=CREATE_NEW_CONSOLE))
        print(' Запущено', count_clients)

    elif user_input == 'x':
        for pr in pr_list:
            pr.kill()
        pr_list.clear()



