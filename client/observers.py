from abc import abstractmethod
from typing import List


class Notifier:
    """
    Интферфейс издателя объявляет набор методов для управлениями подпискичами.
    """

    _state = None
    _listeners: List['Listener'] = []

    def __init__(self, obj) -> None:
        self.employer = obj

    def add_listener(self, listener: 'Listener') -> None:
        """Присоединяет наблюдателя к издателю."""
        self._listeners.append(listener)

    def remove_listener(self, listener: 'Listener') -> None:
        """Отсоединяет наблюдателя от издателя."""
        self._listeners.remove(listener)

    def notify(self, *args, **kwargs) -> None:
        """Уведомляет всех наблюдателей о событии."""
        for listener in self._listeners:
            listener.update(self, *args, **kwargs)


class Listener:
    """
    Интерфейс Наблюдателя объявляет метод уведомления, который издатели
    используют для оповещения своих подписчиков.
    """

    def __init__(self, obj):
        self.employer = obj
        super().__init__()

    @abstractmethod
    def update(self, notifier: Notifier, *args, **kwargs) -> None:
        """Получить обновление от субъекта."""
        pass


class ConnectionNotifier(Notifier):
    """Издатель оповещает наблюдателей о изменениях."""

    _state: bool = False
    _listeners: List['Listener'] = []


class ConnectionListener(Listener):
    """Слушатели издателя соединения с сервером"""

    def update(self, notifier: Notifier, state: bool) -> None:
        self.employer.emit(state)


class MessageNotifier(Notifier):
    """Издатель оповещает наблюдателей о изменениях."""

    _state: bool = False
    _listeners: List['Listener'] = []


class MessageListener(Listener):
    """Слушатели издателя Клиент"""

    def update(self, notifier: Notifier, data: dict) -> None:
        if self.employer.chat_id == data.get('chat_id'):
            self.employer.incoming_msg_s.emit(data)

