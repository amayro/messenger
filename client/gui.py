import sys
from datetime import datetime
from os import listdir
from os.path import abspath, join, isfile

from PyQt5.QtCore import Qt, pyqtSignal, QSize
from PyQt5.QtGui import QIcon, QPixmap, QFont
from PyQt5.QtWidgets import (
    QHBoxLayout,
    QSpacerItem,
    QSizePolicy,
    QFormLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QGroupBox,
    QVBoxLayout,
    QListWidget,
    QTextEdit,
    QWidget,
    QApplication,
    QDialog,
    QAction,
    QToolBar,
    QGridLayout,
    QToolButton,
    QMenu
)

import settings
from html_parser import HtmlToShortTag
from observers import MessageListener, ConnectionListener
from protocol import RequestHandler


class ClientGui(QWidget):
    """Основное окно клиента"""

    def __init__(self, client):
        super().__init__()
        self.client = client
        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('Messenger')
        self.setFixedSize(400, 300)
        self.move(900, 100)

        # Форма settings
        self.start_layout = QVBoxLayout(self)
        self.start_layout.setContentsMargins(70, 20, 70, 20)

        title = QLabel('<b>MESSENGER</b>')

        login_label = QLabel('Login:')
        self.login_input = QLineEdit()

        password_label = QLabel('Password:')
        self.password_input = QLineEdit()
        self.password_input.setEnabled(False)

        self.start_form = QFormLayout()
        self.start_form.addRow(login_label)
        self.start_form.addRow(self.login_input)
        self.start_form.addRow(password_label)
        self.start_form.addRow(self.password_input)
        self.form_group = QGroupBox('', self)
        self.form_group.setContentsMargins(50, 20, 50, 20)
        self.form_group.setLayout(self.start_form)

        self.login_btn = QPushButton('Log in')
        self.login_btn.setMinimumHeight(50)
        self.login_btn.clicked.connect(self.log_in)
        self.login_btn.setAutoDefault(True)
        self.login_input.returnPressed.connect(self.login_btn.click)

        self.register_btn = QPushButton('Sign up')
        self.register_btn.setMinimumHeight(50)
        self.register_btn.setEnabled(False)
        self.register_btn.clicked.connect(self.register)


        h_btn_group_layout = QHBoxLayout()
        h_btn_group_layout.addWidget(self.login_btn)
        h_btn_group_layout.addWidget(self.register_btn)

        self.start_layout.addWidget(title, 0, Qt.AlignCenter)
        self.start_layout.addWidget(self.form_group)
        self.start_layout.addItem(QSpacerItem(0, 20, QSizePolicy.Minimum, QSizePolicy.Minimum))
        self.start_layout.addLayout(h_btn_group_layout)

    def log_in(self):
        """Запускает клиента"""
        self.client.start()

        username = self.login_input.text()
        request = RequestHandler.login(username)

        response = self.client.send_and_get_response(request)

        if response.code == 200:
            RequestHandler.username = username
            self.client.username = username

            self.close()
            dialog = ClientContacts(self.client)
            dialog.show()
            dialog.exec_()
            self.log_out()

    def log_out(self):
        """Выход клиента"""
        pass

    def register(self):
        """Выход клиента"""


class ClientContacts(QDialog):

    is_connect = pyqtSignal(bool)

    def __init__(self, client):
        super().__init__()
        self.client = client
        self.init_ui()

        self.get_contacts()

        self.client.notifier_connect.add_listener(ConnectionListener(self.is_connect))

    def init_ui(self):
        self.setWindowTitle('Messenger')
        self.resize(200, 600)
        self.setFixedWidth(200)
        self.move(900, 100)

        username_label = QLabel(f'<b>Username:</b> {self.client.username}')
        contacts_label = QLabel('Contact list')
        self.statusbar_label = QLabel('Connected')

        self.contact_list = QListWidget()
        self.contact_list.itemDoubleClicked.connect(self.open_chat)

        self.contact_input = QLineEdit()
        self.contact_input.setPlaceholderText('Input contact name for create..')

        self.create_contact_btn = QPushButton('')
        self.create_contact_btn.setFixedHeight(46)
        self.create_contact_btn.clicked.connect(self.create_contact)

        icon_folder = join(settings.IMAGE_ROOT, 'icons')
        icon_add = QIcon()
        icon_add.addPixmap(QPixmap(join(icon_folder, 'add_contact.png')), QIcon.Normal, QIcon.Off)
        self.create_contact_btn.setIcon(icon_add)
        self.create_contact_btn.setIconSize(QSize(32, 32))
        self.create_contact_btn.setAutoDefault(True)
        self.contact_input.returnPressed.connect(self.create_contact_btn.click)

        self.delete_contact_btn = QPushButton('')
        self.delete_contact_btn.setFixedHeight(46)
        self.delete_contact_btn.clicked.connect(self.delete_contact)
        icon_del = QIcon()
        icon_del.addPixmap(QPixmap(join(icon_folder, 'del_contact.png')), QIcon.Normal, QIcon.Off)
        self.delete_contact_btn.setIcon(icon_del)
        self.delete_contact_btn.setIconSize(QSize(32, 32))

        v_contacts_layout = QVBoxLayout()
        v_contacts_layout.addWidget(username_label)
        v_contacts_layout.addItem(QSpacerItem(0, 20, QSizePolicy.Minimum, QSizePolicy.Minimum))

        v_contacts_layout.addWidget(self.contact_input)
        h_btns_layout = QHBoxLayout()
        h_btns_layout.addWidget(self.create_contact_btn)
        h_btns_layout.addWidget(self.delete_contact_btn)
        v_contacts_layout.addLayout(h_btns_layout)
        v_contacts_layout.addWidget(contacts_label, 0, Qt.AlignCenter)
        v_contacts_layout.addWidget(self.contact_list)
        v_contacts_layout.addWidget(self.statusbar_label, 0, Qt.AlignCenter)

        self.setLayout(v_contacts_layout)

        self.is_connect.connect(lambda x: self.statusbar_label.setText('Connected') if x
                                else self.statusbar_label.setText('Disconnected'))

    def closeEvent(self, QCloseEvent):
        """Перегрузка метода для закрытия приложения"""

        super().closeEvent(QCloseEvent)
        sys.exit()

    def open_chat(self, item):
        chat = ChatWindow(self.client, item.text())
        chat.show()
        chat.exec_()

    def get_contacts(self):
        """Обновление окна вывода клиентов"""

        request = RequestHandler.get_contacts()

        response = self.client.send_and_get_response(request)

        if response.code == 200:
            self.contact_list.addItems(response.data)
        else:
            self.statusbar_label.setText(response.info)

    def create_contact(self):
        """Добавляет контак"""

        contact_name = self.contact_input.text()

        if not contact_name:
            return self.statusbar_label.setText('Input contact name, please')
        request = RequestHandler.create_contact(contact_name)

        response = self.client.send_and_get_response(request)

        if response.code == 200:
            self.contact_list.addItem(contact_name)

        self.statusbar_label.setText(response.info)

    def delete_contact(self):
        """Удаляет контакт"""

        selected_item = self.contact_list.currentItem()
        selected_row = self.contact_list.currentRow()
        contact_name = selected_item.text() if selected_item else None

        if not contact_name:
            return self.statusbar_label.setText('Select contact to delete, please')
        request = RequestHandler.delete_contact(contact_name)

        response = self.client.send_and_get_response(request)

        if response.code == 200:
            self.contact_list.takeItem(selected_row)

        self.statusbar_label.setText(response.info)


class ChatWindow(QDialog):

    incoming_msg_s = pyqtSignal(dict)

    def __init__(self, client, contact_name):
        super().__init__()
        self.client = client
        self.contact_name = contact_name
        self.chat_name = f'Chat with user {contact_name}'
        self.icon_folder = join(settings.IMAGE_ROOT, 'icons')
        self.smile_folder = join(settings.IMAGE_ROOT, 'smiles')
        self.html_parser = HtmlToShortTag()

        self.init_ui()

        self.chat_id = self.get_chat()
        self.get_chat_msg()

        self.client.notifier_msg.add_listener(MessageListener(self))

    def init_ui(self):
        self.setWindowTitle(self.chat_name)
        self.resize(600, 600)
        self.move(100, 100)

        self.messages_text_w = QTextEdit()
        self.messages_text_w.setReadOnly(True)
        self.incoming_msg_s.connect(self.receive_message)

        tool_b = QToolBar('Formatting')

        self.bold_btn = QAction(QIcon(join(self.icon_folder, 'bold.jpg')), 'Bold', tool_b)
        self.bold_btn.triggered.connect(self.set_text_format(self.bold_btn))
        self.bold_btn.setCheckable(True)

        self.italic_btn = QAction(QIcon(join(self.icon_folder, 'italic.jpg')), 'Italic', tool_b)
        self.italic_btn.triggered.connect(self.set_text_format(self.italic_btn))
        self.italic_btn.setCheckable(True)

        self.underlined_btn = QAction(QIcon(join(self.icon_folder, 'underlined.jpg')), 'Underlined', tool_b)
        self.underlined_btn.triggered.connect(self.set_text_format(self.underlined_btn))
        self.underlined_btn.setCheckable(True)

        self.smiles_btn = QToolButton()
        self.smiles_btn.setIcon(QIcon(join(self.smile_folder, 'blum1.gif')))
        self.smiles_btn.setMenu(self.get_smiles())
        self.smiles_btn.clicked.connect(self.smiles_btn.showMenu)

        tool_b.addAction(self.bold_btn)
        tool_b.addAction(self.italic_btn)
        tool_b.addAction(self.underlined_btn)
        tool_b.addWidget(self.smiles_btn)

        self.message_input = QTextEdit()
        self.message_input.setMaximumHeight(70)

        self.send_message_btn = QPushButton('Send')
        self.send_message_btn.setMinimumHeight(50)
        self.send_message_btn.clicked.connect(self.send_message)

        v_layout = QVBoxLayout()
        v_layout.addWidget(self.messages_text_w)
        v_layout.addWidget(tool_b)
        v_layout.addWidget(self.message_input)
        v_layout.addWidget(self.send_message_btn)
        self.setLayout(v_layout)

    def get_smiles(self):
        """Открывает окно со смайлами"""

        smiles_folder = self.smile_folder
        smiles = [name for name in listdir(abspath(smiles_folder)) if isfile(join(smiles_folder, name))]
        menu = QMenu()
        layout = QGridLayout()
        menu.setLayout(layout)

        count = 0
        for row in range(len(smiles)):
            for column in range(5):
                try:
                    smile = join(smiles_folder, smiles[count])
                    icon = QIcon(smile)
                    button = QToolButton(self)
                    button.setFixedSize(QSize(32, 32))
                    button.setIcon(icon)
                    button.setIconSize(button.size())
                    button.setAutoRaise(True)
                    button.setToolTip(smiles[count].split('.')[0])
                    button.clicked.connect(self.insert_smile(smile, button.parent()))
                    layout.addWidget(button, row, column)
                    count += 1

                except IndexError:
                    break

        return menu

    @staticmethod
    def set_text_format(button):
        """Производит форматирование текста"""

        def text_format():
            chat = button.parent().parent()

            if button is chat.bold_btn and chat.message_input.fontWeight() == QFont.Normal:
                chat.message_input.setFontWeight(QFont.Black)
            elif button is chat.bold_btn and chat.message_input.fontWeight() == QFont.Black:
                chat.message_input.setFontWeight(QFont.Normal)
            elif button is chat.italic_btn:
                chat.message_input.setFontItalic(not chat.message_input.fontItalic())
            elif button is chat.underlined_btn:
                chat.message_input.setFontUnderline(not chat.message_input.fontUnderline())

        return text_format

    @staticmethod
    def insert_smile(smile, chat):
        """Вставляет смайл в окно ввода"""

        def insert():
            format = chat.message_input.currentCharFormat()
            chat.message_input.textCursor().insertImage(smile)
            chat.message_input.setCurrentCharFormat(format)
            chat.smiles_btn.menu().hide()
        return insert

    def get_chat(self):
        """Возвращает id чата"""

        request = RequestHandler.get_private_chat(self.contact_name)
        response = self.client.send_and_get_response(request)

        return response.data

    def get_chat_msg(self):
        """Получает все сообщения чата"""

        request = RequestHandler.get_chat_msg(self.chat_id)
        response = self.client.send_and_get_response(request)
        if response.data:
            for msg in response.data:
                self.receive_message(msg)

    def send_message(self):
        """Отправка сообщения"""
        msg = self.message_input.toPlainText().rstrip()
        if not msg:
            return

        self.html_parser.feed(self.message_input.toHtml())
        msg = self.html_parser.tagged_message

        self.message_input.clear()
        request = RequestHandler.send_message(self.chat_id, msg)
        self.client.send_request(request)

    def receive_message(self, data: dict):
        """Получение сообщения"""

        time_msg = datetime.fromtimestamp(data.get('time'))
        from_username = data.get('username')
        set_msg_from = 'You' if self.client.username == from_username else from_username
        msg = f"[{time_msg}] <b>{set_msg_from}</b>: {data.get('text')}"
        self.messages_text_w.append(msg)


if __name__ == '__main__':
    from main import Client

    client = Client(host=settings.HOST,
                    port=settings.PORT,
                    buffersize=settings.BUFFERSIZE,
                    encoding_name=settings.ENCODING_NAME)

    client.username = '1'
    RequestHandler.username = '1'
    client.start()
    app = QApplication([])
    gui = ClientContacts(client)
    gui.show()

    sys.exit(app.exec_())
