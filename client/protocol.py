from datetime import datetime
from settings import (
    DATE_ROUTE,
    ECHO_ROUTE,
    USER_ROUTE,
    CHAT_ROUTE,
    MSG_ROUTE,
    CONTACT_ROUTE,
)


class RequestHandler:
    """Класс запроса серверу"""

    username = None

    def __init__(self, action, **kwargs):
        self.action = action
        self.time = int(datetime.now().timestamp())
        self.username = self.username

        [setattr(self, attr, value) for attr, value in kwargs.items()]

    def make(self) -> dict:
        """Формирует запрос от клиента"""

        return {attr: getattr(self, attr, None) for attr in self.__dict__}

    @staticmethod
    def login(username):
        action_route = USER_ROUTE
        action_name = 'login'

        data = {
            'username': username,
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def join_chat(chat_id):
        action_route = CHAT_ROUTE
        action_name = 'join'

        data = {
            'chat_id': chat_id,
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def get_contacts():
        action_route = CONTACT_ROUTE
        action_name = 'get_all'

        data = {}
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def create_contact(contact_name):
        action_route = CONTACT_ROUTE
        action_name = 'create'

        data = {
            'contact_name': contact_name
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def delete_contact(contact_name):
        action_route = CONTACT_ROUTE
        action_name = 'delete'

        data = {
            'contact_name': contact_name
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def send_message(chat_id, text):
        action_route = MSG_ROUTE
        action_name = 'send'

        data = {
            'chat_id': chat_id,
            'text': text,
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def get_private_chat(contact_name):
        action_route = CHAT_ROUTE
        action_name = 'get_private'

        data = {
            'contact_name': contact_name
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()

    @staticmethod
    def get_chat_msg(chat_id):
        action_route = CHAT_ROUTE
        action_name = 'get_chat_msg'

        data = {
            'chat_id': chat_id
        }
        return RequestHandler(action=f'{action_route}/{action_name}', **data).make()


class ResponseHandler:
    """Класс response от сервера."""

    data = {}

    def __init__(self, **kwargs):
        [setattr(self, attr, value) for attr, value in kwargs.items()]

    def as_dict(self) -> dict:
        """Формирует запрос в виде словаря"""

        return {attr: getattr(self, attr, None) for attr in self.__dict__}

    def __repr__(self):
        return str(self.as_dict())
