import dis
import json
import logging
import socket
import sys
import threading
import zlib
from collections import deque

from PyQt5.QtWidgets import QApplication

from observers import MessageNotifier, ConnectionNotifier
from gui import ClientGui
from logger import get_logger
from protocol import ResponseHandler
from settings import ENCODING_NAME, HOST, PORT, BUFFERSIZE


class ClientVerifierMeta(type):

    def __init__(cls, clsname, bases, clsdict):
        cls.__instance = None
        cls.verify_socket(clsname, clsdict)
        type.__init__(cls, clsname, bases, clsdict)

    def __call__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__call__(*args, **kwargs)
        return cls.__instance

    @staticmethod
    def verify_socket(clsname, clsdict):
        """
        Проверяет Client socket на:
        - отсутствие создания сокетов на уровне классов
        - отсутствие вызовов методов сервера (listen, accept)
        - использование TCP соединения в сокетах
        """

        socket_store = None
        for key, value in clsdict.items():
            if isinstance(value, socket.socket):
                raise AttributeError('Creating sockets at the class level is forbidden')

            instructions = dis.get_instructions(value)
            for inst in instructions:
                if inst.argval == 'socket' and inst.opname == 'LOAD_GLOBAL':
                    while inst.opname != 'STORE_ATTR':
                        inst = next(instructions)
                        if inst.opname == 'LOAD_ATTR' and inst.arg == 2 and inst.argval != 'SOCK_STREAM':
                            raise AttributeError('Only TCP connection is available!')
                    socket_store = inst.argval

        if socket_store:
            for key, value in clsdict.items():
                forbidden_methods = ['listen', 'accept']
                instructions = dis.get_instructions(value)
                for inst in instructions:
                    if inst.argval == socket_store:
                        next_inst = next(instructions)
                        if next_inst.argval in forbidden_methods and next_inst.opname == 'LOAD_METHOD':
                            raise AttributeError(f"{clsname} socket should not call method '{next_inst.argval}'")


class Client(metaclass=ClientVerifierMeta):
    def __init__(self, host, port, buffersize, encoding_name):
        self.host = host
        self.port = port
        self.buffersize = buffersize
        self.encoding_name = encoding_name

        self.sock = None
        self.logger = get_logger(type(self).__name__, encoding=self.encoding_name)
        self.logger.setLevel(logging.DEBUG)

        self.notifier_msg = MessageNotifier(self)
        self.notifier_connect = ConnectionNotifier(self)

        self.response = deque()

    def make_connection(self):
        """Создает сокет и устанавливает соединение."""

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect((self.host, self.port))

        except ConnectionRefusedError:
            self.logger.error('Server is not responding')
            self.notifier_connect.notify(state=False)
            return False
        else:
            self.logger.info(f'Client started with {self.host}:{self.port}')
            return True

    def receive_response(self):
        """Получает данные от сервера"""

        while True:
            try:
                data = self.sock.recv(self.buffersize)

            except ConnectionResetError:
                self.logger.error('Server is not responding')
                self.notifier_connect.notify(state=False)

            else:
                if data:
                    b_response = zlib.decompress(data)
                    response = ResponseHandler(**json.loads(b_response.decode(self.encoding_name)))
                    self.logger.debug(f'<--- {response.__dict__}')
                    self.logger.info(f'{response.data}')

                    data = response.data
                    if isinstance(data, dict):
                        if data.get('chat_id'):
                            self.notifier_msg.notify(data)
                    else:
                        self.response.append(response)

    def send_request(self, request: dict):
        """Получает данные от пользователя и отправляет на сервер"""

        request = json.dumps(request)
        self.logger.debug(f'---> {request}')
        compressed_request = zlib.compress(request.encode(self.encoding_name))

        try:
            self.sock.send(compressed_request)

        except ConnectionResetError:
            self.logger.error('Server is not responding')

    def send_and_get_response(self, request: dict):
        """Отправляет данные и ожидает ответа"""

        self.send_request(request)
        while not self.response:
            pass

        return self.response.popleft()

    def start(self):
        """Запуск клиента"""

        connection = self.make_connection()
        if connection:
            thr_response = threading.Thread(target=self.receive_response)
            thr_response.daemon = True
            thr_response.start()


if __name__ == '__main__':
    client = Client(host=HOST,
                    port=PORT,
                    buffersize=BUFFERSIZE,
                    encoding_name=ENCODING_NAME)

    app = QApplication([])
    gui = ClientGui(client)
    gui.show()

    sys.exit(app.exec_())




