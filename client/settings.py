import os

BUFFERSIZE = 1024
ENCODING_NAME = 'utf-8'
HOST = 'localhost'
PORT = 8000

DATE_ROUTE = 'dates'
ECHO_ROUTE = 'echo'
USER_ROUTE = 'user'
CHAT_ROUTE = 'chat'
CONTACT_ROUTE = 'contact'
MSG_ROUTE = 'message'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
IMAGE_ROOT = os.path.join(BASE_DIR, 'images')
