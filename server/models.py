from sqlalchemy import Column, DateTime, Integer, func
from sqlalchemy.ext.declarative import declared_attr

from db import SessionContext


class Core:
    """Общее ядро для таблиц"""

    @declared_attr
    def __tablename__(cls):
        return f'{str(cls.__name__.lower())}'

    id = Column(Integer, primary_key=True)
    created_dt = Column(DateTime, default=func.now(), nullable=False)
    updated_dt = Column(DateTime, default=func.now(), nullable=False, onupdate=func.now())

    @classmethod
    def create(cls, session, *args, **kwargs):
        """Добавить в базу"""

        with SessionContext(session) as session:
            [kwargs.pop(kw) for kw in list(kwargs.keys()) if not hasattr(cls, kw)]
            obj = cls(*args, **kwargs)
            session.add(obj)
            session.commit()
        return obj

    def update(self, session, **kwargs):
        """Обновление данных экземпляра"""
        with SessionContext(session) as session:
            [kwargs.pop(kw) for kw in list(kwargs.keys()) if not hasattr(self, kw)]
            [setattr(self, key, value) for key, value in kwargs.items() if value]
            session.add(self)
            session.commit()

    def delete(self, session):
        """Удалить из базы"""
        with SessionContext(session) as session:
            session.delete(self)
            session.commit()

    @classmethod
    def all(cls, session):
        """Получить все объекты модели"""

        with SessionContext(session) as _session:
            return _session.query(cls).all()

    @classmethod
    def get(cls, session, obj_id):
        """
        Получить объект класса cls из базы.
        Если объекта с obj_id в базе не существует, возвращается None.
        """

        obj = session.query(cls).filter(cls.id == obj_id).first()
        return obj
