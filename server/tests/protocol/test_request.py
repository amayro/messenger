from datetime import datetime

import pytest

from protocol import RequestHandler


@pytest.fixture
def valid_request():
    return {
        'action': 'upper_text',
        'time': int(datetime.now().timestamp()),
        'username': 'any_nick or None',
    }


@pytest.fixture
def invalid_request():
    return {}


def test_validate_request_success(valid_request):
    """Тест проверки запроса на валидность"""
    assert RequestHandler(**valid_request).is_valid() is True


def test_validate_request_fail(invalid_request):
    """Тест проверки запроса на невалидность"""
    assert RequestHandler(**invalid_request).is_valid() is False
