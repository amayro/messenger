from datetime import datetime

import pytest

from protocol import ResponseHandler, RequestHandler


@pytest.fixture
def any_username():
    return None


@pytest.fixture
def any_action():
    return 'any_action'


@pytest.fixture
def any_data():
    return {
        'data': 'Data in request'
    }


@pytest.fixture
def user_request(any_action, any_username):
    return RequestHandler(**{
        'action': any_action,
        'time': int(datetime.now().timestamp()),
        'username': any_username,
    })


@pytest.fixture
def expect_response(any_action, any_data, any_username):
    return {
        'action': any_action,
        'data': any_data,
        'time': int(datetime.now().timestamp()),
        'code': 200,
        'info': 'OK',
    }


def test_data_for_make(user_request, any_data, expect_response):
    """Тест формирования ответа"""

    response = ResponseHandler(user_request, any_data).make()
    time_expected = expect_response.pop('time')
    time_response = response.pop('time')

    assert response == expect_response
    assert str(time_response)[:8] == str(time_expected)[:8]
