import pytest
from routes import resolve


@pytest.fixture
def controller():
    return lambda arg: arg


@pytest.fixture
def routes(controller):
    return [
        {'action': 'echo', 'controller': controller},
    ]


def test_resolve(routes, controller):
    """Тест с вымыщленным роутом и контролллером"""
    assert resolve('echo', routes) == controller


def test_resolve_fail():
    """Тест несуществующего роута"""
    assert resolve('echo', [{}]) is None


def test_resolve_exists_controller():
    """Тест существующего контроллера"""
    assert resolve('dates/now').__name__ == 'get_date_now'
    assert resolve('now', app_routes=False).__name__ == 'get_date_now'
