from routes import get_server_routes


def test_get_server_routes_type():
    """Получение всех доступных роутов из приложений"""

    assert isinstance(get_server_routes(), list)
    assert isinstance(get_server_routes()[0], dict)
