import pytest

from decorators import need_attributes
from protocol import ResponseHandler, RequestHandler


@pytest.fixture
def request_():
    data = {
        'username': 'nick',
        'chat_id': None,
        'action': 'any_action',
    }
    return RequestHandler(**data)


def test_need_attributes_valid(request_):
    @need_attributes(['username', 'action'])
    def some_func(request):
        return ResponseHandler(request).make()

    assert some_func(request_) == ResponseHandler(request_).make()


def test_need_attributes_invalid(request_):
    @need_attributes(['username', 'msg_from'])
    def some_func(request):
        return ResponseHandler(request).make()

    assert some_func(request_) == ResponseHandler(request_).make_400()
