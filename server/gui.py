import logging
import threading

from PyQt5.QtCore import Qt, pyqtSignal, QObject, pyqtSlot
from PyQt5.QtWidgets import (
    QHBoxLayout,
    QSpacerItem,
    QSizePolicy,
    QFormLayout,
    QLabel,
    QLineEdit,
    QPushButton,
    QGroupBox,
    QVBoxLayout,
    QListWidget,
    QWidget,
    QPlainTextEdit)

import settings
from observers import ClientListener


class ThreadLogger(logging.Handler):
    """Класс перехватывания логгов сервера в окно для вывода"""
    def __init__(self):
        super().__init__()
        self.log = MyLog()

        self.setFormatter(
            logging.Formatter(
                '[%(levelname)s] [%(asctime)s]: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        )
        logging.getLogger('Server').addHandler(self)

    def emit(self, record):
        msg = self.format(record)
        self.log.signal.emit(msg)


class MyLog(QObject):
    """Создает новый сигнал для логов, чтобы избежать падения программы"""
    signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()


class ServerGui(QWidget):
    """Основное окно сервера"""

    update_client_signal = pyqtSignal(list)

    def __init__(self, server):
        super().__init__()
        self.server = server
        self.init_ui()

        self.set_settings_field(
            host=self.server.host,
            port=self.server.port,
            encoding_name=self.server.encoding_name,
            buffersize=self.server.buffersize,
        )

        self.server.notifier_client.add_listener(ClientListener(self.update_client_signal))
        self.start_server()

    def init_ui(self):
        self.setWindowTitle('Admin panel')
        self.resize(900, 500)
        self.move(750, 450)

        h_layout = QHBoxLayout(self)

        # Форма settings
        self.settings_form = QFormLayout()
        host_label = QLabel('Host:')
        port_label = QLabel('Port:')
        encoding_label = QLabel('Encoding name:')
        buffersize_label = QLabel('Buffersize:')

        self.host_input = QLineEdit()
        self.port_input = QLineEdit()
        self.encoding_input = QLineEdit()
        self.buffersize_input = QLineEdit()


        self.settings_default_btn = QPushButton('Default settings')
        self.settings_default_btn.setFixedSize(100, 30)
        self.settings_default_btn.clicked.connect(self.set_settings_field)

        self.settings_form.addRow(host_label)
        self.settings_form.addRow(self.host_input)
        self.settings_form.addRow(port_label)
        self.settings_form.addRow(self.port_input)
        self.settings_form.addRow(encoding_label)
        self.settings_form.addRow(self.encoding_input)
        self.settings_form.addRow(buffersize_label)
        self.settings_form.addRow(self.buffersize_input)
        self.settings_form.addItem(QSpacerItem(0, 20, QSizePolicy.Minimum, QSizePolicy.Maximum))
        h_settings_default_lay = QHBoxLayout()
        h_settings_default_lay.addWidget(self.settings_default_btn, 0, Qt.AlignCenter)
        self.settings_form.addRow(h_settings_default_lay)

        self.settings_group = QGroupBox('Settings', self)
        self.settings_group.setFixedWidth(200)
        self.settings_group.setContentsMargins(10, 20, 10, 20)
        self.settings_group.setLayout(self.settings_form)

        # Кнопки управления сервером
        self.start_server_btn = QPushButton('Start server')
        self.start_server_btn.setMinimumHeight(50)
        self.start_server_btn.clicked.connect(self.start_server)

        self.stop_server_btn = QPushButton('Stop server')
        self.stop_server_btn.setMinimumHeight(50)
        self.stop_server_btn.setEnabled(False)
        self.stop_server_btn.clicked.connect(self.stop_server)

        self.clear_log_btn = QPushButton('Clear logs')
        self.clear_log_btn.setMinimumHeight(50)
        self.clear_log_btn.clicked.connect(self.clear_log)

        h_btn_group_layout = QHBoxLayout()
        h_btn_group_layout.addWidget(self.stop_server_btn)
        h_btn_group_layout.addWidget(self.clear_log_btn)

        # Установка общего поля для настроек и кнопок запуска/остановки
        v_settings_layout = QVBoxLayout()
        v_settings_layout.addItem(QSpacerItem(0, 12, QSizePolicy.Minimum, QSizePolicy.Minimum))
        v_settings_layout.addWidget(self.settings_group)

        v_settings_layout.addItem(QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding))
        v_settings_layout.addWidget(self.start_server_btn)
        v_settings_layout.addLayout(h_btn_group_layout)

        self.clients_list = QListWidget()
        self.clients_list.setMaximumWidth(200)

        # Установка поля вывода клиентов
        v_clients = QVBoxLayout()
        clients_label = QLabel('<b>Clients</b>')
        v_clients.addWidget(clients_label, 0, Qt.AlignCenter)
        v_clients.addWidget(self.clients_list)
        self.update_client_signal.connect(self.update_clients)

        # Установка поля для вывода логов
        v_log_layout = QVBoxLayout()
        log_label = QLabel('<b>Log messages</b>')
        self.log_box = QPlainTextEdit()
        self.log_box.setReadOnly(True)
        self.log_box.setLineWrapMode(self.log_box.NoWrap)
        v_log_layout.addWidget(log_label, 0, Qt.AlignCenter)
        v_log_layout.addWidget(self.log_box)
        log_handler = ThreadLogger()
        log_handler.log.signal.connect(self.write_log)
        self.update_client_signal.connect(lambda x: self.log_box.appendPlainText(f"All Clients {len(x)}: {str(x)}"))

        # Добавление всех вертикальных полей на главное горизонтальное поле
        h_layout.addLayout(v_settings_layout)
        h_layout.addLayout(v_clients)
        h_layout.addLayout(v_log_layout)

    @pyqtSlot(str)
    def write_log(self, log_text):
        """Вывод логов в админ панеле"""

        self.log_box.appendPlainText(log_text)
        self.log_box.centerCursor()

    def update_clients(self, lst_client: list):
        """Обновление окна вывода клиентов"""

        self.clients_list.clear()
        for client in lst_client:
            self.clients_list.addItem(client)

    def clear_log(self):
        """Очищает окно вывода логов"""

        self.log_box.clear()

    def start_server(self):
        """Запускает сервер"""

        self.start_server_btn.setEnabled(False)
        self.stop_server_btn.setEnabled(True)
        self.update_settings()

        thr_server = threading.Thread(target=self.server.start)
        thr_server.daemon = True
        thr_server.start()

    def stop_server(self):
        """Останавливает сервер"""

        self.start_server_btn.setEnabled(True)
        self.stop_server_btn.setEnabled(False)

        self.server.close()

    def update_settings(self):
        """Обновляет настройки согласно полям ввода"""

        self.server.host = self.host_input.text()
        self.server.port = int(self.port_input.text())
        self.server.encoding_name = self.encoding_input.text()
        self.server.buffersize = int(self.buffersize_input.text())

    def set_settings_field(
            self,
            *args,
            host=settings.HOST,
            port=settings.PORT,
            encoding_name=settings.ENCODING_NAME,
            buffersize=settings.BUFFERSIZE,
    ):
        """
        Устанавливает значения в поля ввода, пришедшие при запуске (из config.yaml или из settings),
        либо стандартные из settings по нажатию кнопки

        **Args**:
        *args: - при срабатывании сигнала нажатия кнопки передается еще checked=False,
        """

        self.host_input.setText(str(host))
        self.port_input.setText(str(port))
        self.encoding_input.setText(str(encoding_name))
        self.buffersize_input.setText(str(buffersize))
