import os
from pathlib import PurePath

from .controllers import (
    get_echo
)

routes = [
    {'action': 'echo', 'controller': get_echo},
]

app_name = PurePath(os.path.dirname(__file__)).parts[-1].rstrip('app')
app_routes = [
    {'action': f"{app_name}/{route.get('action')}",
     'controller': route.get('controller')}
    for route in routes
]
