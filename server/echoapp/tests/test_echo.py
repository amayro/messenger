from datetime import datetime

from echoapp.controllers import get_echo
from protocol import RequestHandler


def request_data(data):
    """Входящие данные"""
    return RequestHandler(**{
        'action': 'upper_text',
        'time': int(datetime.now().timestamp()),
        'data': data
    })


def response_data(data):
    """Ожидаемое значение данных"""

    request = request_data(data)
    return get_echo(request)


def test_get_echo_valid():
    """Тест корректного эхо ответа"""

    data = 'Some data'
    assert response_data(data).get('data') == data


def test_get_echo_invalid():
    """Тест невалидного ответа эхо"""

    data = None
    assert response_data(data).get('data') == data
    assert response_data(data).get('info') == 'Wrong request format'
