from decorators import logged
from protocol import ResponseHandler, RequestHandler


@logged
def get_echo(request: RequestHandler):
    """Отправка эхо"""

    data = request.data
    response = ResponseHandler(request, data)
    if data:
        return response.make()

    return response.make_400()
