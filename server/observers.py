import re
from abc import abstractmethod
from typing import List


class Notifier:
    """
    Интферфейс издателя объявляет набор методов для управлениями подпискичами.
    """

    _state = None
    _listeners: List['Listener'] = []

    def __init__(self, obj) -> None:
        self.employer = obj

    def add_listener(self, listener: 'Listener') -> None:
        """Присоединяет наблюдателя к издателю."""
        self._listeners.append(listener)

    def remove_listener(self, listener: 'Listener') -> None:
        """Отсоединяет наблюдателя от издателя."""
        self._listeners.remove(listener)

    def notify(self, *args, **kwargs) -> None:
        """Уведомляет всех наблюдателей о событии."""
        for listener in self._listeners:
            listener.update(self, *args, **kwargs)


class Listener:
    """
    Интерфейс Наблюдателя объявляет метод уведомления, который издатели
    используют для оповещения своих подписчиков.
    """

    def __init__(self, obj):
        self.employer = obj
        super().__init__()

    @abstractmethod
    def update(self, notifier: Notifier, *args, **kwargs) -> None:
        """Получить обновление от субъекта."""
        pass


class ClientNotifier(Notifier):
    """Издатель оповещает наблюдателей о изменениях."""

    _state: bool = False
    _listeners: List['ClientListener'] = []

    def notify(self, lst_client: List) -> None:
        """Запуск обновления в каждом подписчике."""

        lst_client = self._change_list_clients(lst_client)
        super().notify(lst_client)

    @staticmethod
    def _change_list_clients(lst_client):
        """Изменяет формат имен клиентов"""

        changed_lst = []
        for client in lst_client:
            address = re.findall('raddr=\(\'(.*)\)>', str(client))[0]
            address = address.replace("', ", ':')
            changed_lst.append(address)
        return changed_lst


class ClientListener(Listener):
    """Слушатели издателя Клиент"""

    def update(self, notifier: Notifier, lst_client) -> None:
        self.employer.emit(lst_client)
