import logging
import zlib
from functools import wraps

from protocol import ResponseHandler

logger = logging.getLogger('Server')


def logged(func):
    """Декоратор для отображения имени функции и запроса при логгировании в режиме debug"""

    @wraps(func)
    def wrapper(request, *args, **kwargs):
        logger.debug(f'{func.__name__} - {request}')
        return func(request, *args, **kwargs)

    return wrapper


def login_required(func):
    """Декоратор проверки доступа для пользователя"""

    @wraps(func)
    def wrapper(request, *args, **kwargs):
        user = request.username
        logger.debug(user)
        if user:
            return func(request, *args, **kwargs)

        return ResponseHandler(request).make_403()

    return wrapper


def compressed(method):
    """Декоратор декомпрессии входящих сжатых данных для работы функции,
    с последующим сжатием данных отработавшей функции"""

    @wraps(method)
    def wrapper(self, request, *args, **kwargs):
        if request:
            b_request = zlib.decompress(request)
            s_response = method(self, b_request, *args, **kwargs)
            return zlib.compress(s_response)
        else:
            return method(self, request, *args, **kwargs)

    return wrapper


def need_attributes(lst_attrs):
    def actual_decorator(func):
        """Декоратор проверки необходимых атрибутов у объекта"""

        @wraps(func)
        def wrapper(request, *args, **kwargs):
            if all([hasattr(request, obj) for obj in lst_attrs]):
                return func(request, *args, **kwargs)

            return ResponseHandler(request).make_400()

        return wrapper
    return actual_decorator

