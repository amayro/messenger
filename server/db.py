import importlib.util
import inspect

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from settings import INSTALLED_MODULES

Base = declarative_base()


class SingletonMeta(type):
    """Реализация Singleton"""

    def __init__(cls, *args, **kwargs):
        cls.__instance = None
        super().__init__(*args, **kwargs)

    def __call__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__call__(*args, **kwargs)
        return cls.__instance


class Singleton(metaclass=SingletonMeta):
    """Базовый класс Singleton"""
    pass


class ServerDBError(Exception):
    """Класс-исключение для ServerDB"""
    pass


class ServerDB(Singleton):
    """Класс для упраления БД сервера"""

    def __init__(self, db_name=None):
        if not db_name:
            db_name = "sqlite:///:memory:"
        else:
            db_name = f"sqlite:///{db_name}"

        self.engine = create_engine(db_name, echo=False, connect_args={'check_same_thread': False})
        self.base = Base
        self.session = None

        self.setup()

    def setup(self):
        """Загрузка БД"""

        [importlib.import_module(f'{module}.models')
         for module in INSTALLED_MODULES if importlib.util.find_spec(f'{module}.models')],

        self.base.metadata.create_all(bind=self.engine)
        self.session = sessionmaker(bind=self.engine)()

    def close(self):
        """Закрытие БД"""

        print("DB close")
        if self.session:
            self.session.close()
            self.session = None


class SessionContext:
    """Контекстный менеджер для операций с БД"""

    def __init__(self, session):
        self.session = session

    def __enter__(self):
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            self.session.rollback()
            method_name = inspect.stack()[1][3]
            raise ServerDBError(f'Error in {method_name}')
