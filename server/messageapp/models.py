from sqlalchemy import Column, Integer, ForeignKey, Text

from db import Base
from models import Core


class Message(Core, Base):
    """Модель сообщений пользователя"""

    owner_id = Column(Integer, ForeignKey('user.id'))
    chat_id = Column(Integer, ForeignKey('chat.id'))
    text = Column(Text)

    def __init__(self, owner_id, chat_id, text):
        self.owner_id = owner_id
        self.chat_id = chat_id
        self.text = text

    def __repr__(self):
        return f'<Message(id={self.id}, owner={self.owner_id}, chat={self.chat_id})>'

    @classmethod
    def get_all_for_chat(cls, session, chat_id):
        """Получить все сообщени чата"""

        obj = session.query(cls).filter(cls.chat_id == chat_id).all()
        return obj
