from decorators import need_attributes
from messageapp.models import Message
from protocol import ResponseHandler
from userapp.models import User


@need_attributes(['username', 'chat_id', 'text'])
def send_message(request):
    """Отправка сообщения"""

    user = User.get_by_username(request.session, request.username)
    msg = Message.create(request.session, owner_id=user.id, chat_id=request.chat_id, text=request.text)

    data = {
        'username': request.username,
        'time': int(msg.created_dt.timestamp()),
        'chat_id': request.chat_id,
        'text': request.text
    }
    response = ResponseHandler(request, data).make()
    return response

