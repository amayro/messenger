from contactapp.controllers import create_contact, delete_contact
from db import ServerDB
from protocol import RequestHandler
from userapp.models import User


def request_del_contact(db, user_1, user_2):
    data = {
        "username": user_1.username,
        "contact_name": user_2.username,
    }
    request = RequestHandler(**data)
    request.session = db.session
    return request


def request_cr_contact(db, user_1, user_2):
    data = {
        "username": user_1.username,
        "contact_name": user_2.username,
    }
    request = RequestHandler(**data)
    request.session = db.session
    return request


class TestController:

    def setup(self):
        self.db = ServerDB()
        self.db.setup()

        self.user_1 = User.create(self.db.session, **{'username': 'nick'})
        self.user_2 = User.create(self.db.session, **{'username': 'jack'})

        self.request_cr_cont_12 = request_cr_contact(self.db, self.user_1, self.user_2)

    def teardown(self):
        self.db.close()
        self.db.base.metadata.drop_all(self.db.engine)

    def test_delete_contact_valid(self):
        create_contact(self.request_cr_cont_12)
        response = delete_contact(request_del_contact(self.db, self.user_1, self.user_2))
        assert response.get('code') == 200
        assert response.get('info').startswith('DELETE contact')

    def test_delete_contact_invalid(self):
        response = delete_contact(request_del_contact(self.db, self.user_1, self.user_2))
        assert response.get('code') == 404
        assert response.get('info').startswith('contact NOT found')

