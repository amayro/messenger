from contactapp.controllers import create_contact
from db import ServerDB
from protocol import RequestHandler
from userapp.models import User


def request_cr_contact(db, user_1, user_2):
    data = {
        "username": user_1.username,
        "contact_name": user_2.username,
    }
    request = RequestHandler(**data)
    request.session = db.session
    return request


class TestController:

    def setup(self):
        self.db = ServerDB()
        self.db.setup()

        self.user_1 = User.create(self.db.session, **{'username': 'nick'})
        self.user_2 = User.create(self.db.session, **{'username': 'jack'})
        self.user_3 = User.create(self.db.session, **{'username': 'sara'})

        self.request_cr_1 = request_cr_contact(self.db, self.user_1, self.user_2)
        self.request_cr_2 = request_cr_contact(self.db, self.user_1, self.user_3)

    def teardown(self):
        self.db.close()
        self.db.base.metadata.drop_all(self.db.engine)

    def test_create_contact_valid(self):
        create_1 = create_contact(self.request_cr_1)
        assert create_1.get('code') == 200
        assert create_1.get('info').startswith('CREATE contact')

    def test_create_contact_invalid(self):
        create_contact(self.request_cr_1)
        create_1 = create_contact(self.request_cr_1)
        assert create_1.get('code') == 202
        assert create_1.get('info').startswith('Contact already exists')
