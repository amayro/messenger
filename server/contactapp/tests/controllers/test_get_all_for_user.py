from contactapp.controllers import create_contact
from contactapp.controllers import get_all_for_user
from contactapp.models import Contact
from db import ServerDB
from protocol import RequestHandler
from userapp.models import User


def request_get_all(db, user_1):
    data = {
        "username": user_1.username,
    }
    request = RequestHandler(**data)
    request.session = db.session
    return request


def request_cr_contact(db, user_1, user_2):
    data = {
        "username": user_1.username,
        "contact_name": user_2.username,
    }
    request = RequestHandler(**data)
    request.session = db.session
    return request


class TestController:

    def setup(self):
        self.db = ServerDB()
        self.db.setup()

        self.user_1 = User.create(self.db.session, **{'username': 'nick'})
        self.user_2 = User.create(self.db.session, **{'username': 'jack'})
        self.user_3 = User.create(self.db.session, **{'username': 'sara'})

        self.request_cr_cont_12 = request_cr_contact(self.db, self.user_1, self.user_2)
        self.request_cr_cont_13 = request_cr_contact(self.db, self.user_1, self.user_3)

    def teardown(self):
        self.db.close()
        self.db.base.metadata.drop_all(self.db.engine)

    def test_get_all_for_user_valid(self):
        response = get_all_for_user(request_get_all(self.db, self.user_1))
        assert response.get('data') is None
        assert response.get('code') == 202
        assert response.get('info') == 'contacts list is empty'

        create_contact(self.request_cr_cont_12)
        create_contact(self.request_cr_cont_13)
        response = get_all_for_user(request_get_all(self.db, self.user_1))
        assert response.get('data') == [self.user_2.username, self.user_3.username]
        assert response.get('code') == 200

        response = get_all_for_user(request_get_all(self.db, self.user_2))
        assert response.get('data') is None

        contact_13 = Contact.get_contact(self.db.session, self.user_1.id, self.user_3.id)
        contact_13.delete(self.db.session)
        response = get_all_for_user(request_get_all(self.db, self.user_1))
        assert response.get('data') == [self.user_2.username]
