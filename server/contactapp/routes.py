import os
from pathlib import PurePath

from .controllers import (
    create_contact,
    update_contact,
    delete_contact,
    get_contact,
    get_all_for_user
)

routes = [
    {'action': 'create', 'controller': create_contact},
    {'action': 'update', 'controller': update_contact},
    {'action': 'delete', 'controller': delete_contact},

    {'action': 'get', 'controller': get_contact},
    {'action': 'get_all', 'controller': get_all_for_user},
]

app_name = PurePath(os.path.dirname(__file__)).parts[-1].rstrip('app')
app_routes = [
    {'action': f"{app_name}/{route.get('action')}",
     'controller': route.get('controller')}
    for route in routes
]
