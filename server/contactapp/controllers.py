from decorators import login_required, need_attributes

from protocol import ResponseHandler
from userapp.models import User
from .models import Contact


@need_attributes(['username', 'contact_name'])
@login_required
def create_contact(request):
    """Создает контакт для пользователя"""

    user = User.get_by_username(request.session, request.username)
    user_contact = User.get_by_username(request.session, request.contact_name)

    if user_contact:
        contact = Contact.get_contact(request.session, user.id, user_contact.id)

        if user_contact == user:
            return ResponseHandler(request, data=None).make(400, info='You can\'t add yourself to contacts')
        elif not contact:
            new_contact = Contact.create(request.session, owner_id=user.id, contact_id=user_contact.id)
            return ResponseHandler(request, data=None).make(info=f'CREATE contact success {str(new_contact)}')
        else:
            return ResponseHandler(request, data=None).make(202, info='Contact already exists')

    return ResponseHandler(request, data=None).make(404, info='contact NOT found')


def update_contact(request):
    """Обновляет контакт"""

    pass


@need_attributes(['username', 'contact_name'])
def delete_contact(request):
    """Удаляет контакт"""

    user = User.get_by_username(request.session, request.username)
    user_contact = User.get_by_username(request.session, request.contact_name)

    del_contact = Contact.get_contact(request.session, user.id, user_contact.id)
    if del_contact:
        del_contact.delete(request.session)
        return ResponseHandler(request, data=None).make(info=f'DELETE contact success {str(del_contact)}')

    return ResponseHandler(request, data=None).make(404, info='contact NOT found')


@need_attributes(['username'])
def get_all_for_user(request):
    """Получение всех контактов для пользователя"""

    user = User.get_by_username(request.session, request.username)
    contacts = Contact.get_all_for_user(request.session, user.id)

    data = [User.get(request.session, c.contact_id).username for c in contacts]
    if data:
        return ResponseHandler(request, data=data).make(info=f'GET all contacts for user success')

    return ResponseHandler(request, data=None).make(202, info='contacts list is empty')


@need_attributes(['username', 'contact_name'])
def get_contact(request):
    """Получение одного контакта для пользователя"""

    user = User.get_by_username(request.session, request.username)
    user_contact = User.get_by_username(request.session, request.contact_name)

    g_contact = Contact.get_contact(request.session, user.id, user_contact.id)
    if g_contact:
        return ResponseHandler(
            request,
            data=None
        ).make(info=f'GET one contact for user success {str(g_contact)}')

    return ResponseHandler(request, data=None).make(404, info='contact NOT found')

