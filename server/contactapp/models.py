from sqlalchemy import Column, ForeignKey, UniqueConstraint

from db import Base
from models import Core


class Contact(Core, Base):
    """Модель список контактов"""

    owner_id = Column(ForeignKey('user.id'))
    contact_id = Column(ForeignKey('user.id'))

    __uix = UniqueConstraint(owner_id, contact_id)

    def __init__(self, owner_id, contact_id):
        self.owner_id = owner_id
        self.contact_id = contact_id

    def __repr__(self):
        return f"<Contact(user_id={self.owner_id}, contact_id={self.contact_id})>"

    @classmethod
    def get_all_for_user(cls, session, owner_id):
        """
        Получить все контакты для владельца с заданным id.
        Если объекта в базе не существует, возвращается None.
        """
        return session.query(cls).filter_by(owner_id=owner_id).all()

    @classmethod
    def get_contact(cls, session, owner_id, contact_id):
        """
        Получить контакт для владельца с заданным id.
        Если объекта в базе не существует, возвращается None.
        """
        return session.query(cls).filter_by(owner_id=owner_id, contact_id=contact_id).first()
