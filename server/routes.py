from functools import reduce
from typing import List

from settings import INSTALLED_MODULES


def get_server_routes(app_routes=True) -> List[dict]:
    """
    Получение всех доступных роутов из приложений

    **Args**

    ``app_routes``:
        - если True, тогда будут собираться роуты из переменных app_routes,
          формат которых ['appname/action1', 'appname/action2',...]
        - если False, тогда будут собираться роуты из переменных routes,
          формат которых ['action1', 'action2',...]
    """

    var_routes = 'app_routes' if app_routes else 'routes'

    return reduce(
        lambda routes, module: routes + getattr(module, f'{var_routes}', []),
        reduce(
            lambda submodules, module: submodules + [getattr(module, 'routes', None)],
            reduce(
                lambda modules, module: modules + [__import__(f'{module}.routes')],
                INSTALLED_MODULES,
                []
            ),
            []
        ),
        []
    )


def resolve(action: str, routes: List[dict] = None, app_routes=True):
    """
    Возвращает контроллер для действия из указанного роута.
    Если роуты не указаны, берутся все доступные.
    **Args**

    ``app_routes``: см. get_server_routes()
    """

    routes_mapping = {route.get('action'): route.get('controller') for route in routes or get_server_routes(app_routes)}
    return routes_mapping.get(action, None)
