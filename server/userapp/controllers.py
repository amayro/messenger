from decorators import login_required, need_attributes
from protocol import ResponseHandler
from .models import User


@need_attributes(['username'])
def login(request):
    """Обрабатывает вход пользователя"""

    return get_or_create_user(request)


@need_attributes(['username'])
@login_required
def logout(request):
    """Обрабатывает выход пользователя"""

    pass


def _create_user(request):
    """Создает пользователя"""

    user = User.create(**request.as_dict())

    return ResponseHandler(request, data=user.id).make(info=f'CREATE user success {str(user)}')


@need_attributes(['username'])
def get_or_create_user(request):
    """Возвращает данные пользователя из базы. Если их нет, то сначала создает"""

    user = User.get_by_username(request.session, request.username)
    if not user:
        return _create_user(request)

    return ResponseHandler(request, data=user.id).make(info=f'GET user success {str(user)}')


@need_attributes(['username'])
@login_required
def update_user(request):
    """Обновляет данные пользователя"""

    user = User.get_by_username(request.session, request.username)
    if not user:
        return ResponseHandler(request).make(404, 'User not found')

    user.update(**request.as_dict())

    return ResponseHandler(request).make(info=f'UPDATE user success {str(user)}')


@need_attributes(['username'])
@login_required
def delete_user(request):
    """Удаляет пользователя"""

    user = User.get_by_username(request.session, request.username)
    if not user:
        return ResponseHandler(request).make(404, 'User not found')

    user.delete(request.session)

    return ResponseHandler(request).make(info=f'DELETE user success {str(user)}')


def get_users_all(request):
    """Возвращает данные всех пользователей из базы."""

    users = User.all(request.session)

    return ResponseHandler(
        request,
        data=[{'user_id': user.id, 'username': user.username} for user in users]
    ).make(info=f'GET user success {str(users)}')
