from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy.orm import relationship

from db import Base
from models import Core

assoc_table_userchat = Table(
    'association_UserChat',
    Base.metadata,
    Column('user_id', Integer, ForeignKey('user.id')),
    Column('chat_id', Integer, ForeignKey('chat.id'))
)


class User(Core, Base):
    """Модель пользователя"""

    username = Column(String(24), unique=True, nullable=False)
    first_name = Column(String(32))
    second_name = Column(String(32))
    bio = Column(String(128))
    password = Column(String(32))

    chat = relationship('Chat', secondary=assoc_table_userchat, backref="user")
    message = relationship('Message', order_by='Message.id', backref='owner')
    history = relationship('UserHistory', backref='owner')

    def __init__(
            self,
            username,
            first_name=None,
            second_name=None,
            password=None,
            bio=None,
    ):
        self.username = username
        self.first_name = first_name
        self.second_name = second_name
        self.password = password
        self.bio = bio

    def __repr__(self):
        return f"<User(id={self.id}, " \
            f"username={self.username}, " \
            f"first_name={self.first_name}, " \
            f"second_name={self.second_name}, " \
            f"password={self.password}, " \
            f"bio={self.bio}, " \
            f")>"

    @classmethod
    def get_by_username(cls, session, username):
        """
        Получить объект класса из базы.
        Если объекта с username в базе не существует, возвращается None.
        """

        obj = session.query(cls).filter(cls.username == username).first()
        return obj


class UserHistory(Core, Base):
    """Модель истории пользователя"""

    owner_id = Column(ForeignKey('user.id'))
    ip_address = Column(String)

    def __init__(self, owner_id, ip_address):
        self.owner_id = owner_id
        self.ip_address = '{}:{}'.format(*ip_address)

    def __repr__(self):
        return f"<UserHistory(user_id='{self.owner_id}')>"
