import os
from pathlib import PurePath

from .controllers import (
    login,
    logout,
    get_or_create_user,
    update_user,
    delete_user,
    get_users_all,
)

routes = [
    {'action': 'login', 'controller': login},
    {'action': 'logout', 'controller': logout},

    {'action': 'get_or_create', 'controller': get_or_create_user},
    {'action': 'update', 'controller': update_user},
    {'action': 'delete', 'controller': delete_user},

    {'action': 'get_all', 'controller': get_users_all},
]

app_name = PurePath(os.path.dirname(__file__)).parts[-1].rstrip('app')
app_routes = [
    {'action': f"{app_name}/{route.get('action')}",
     'controller': route.get('controller')}
    for route in routes
]
