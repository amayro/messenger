from datetime import datetime


class RequestHandler:
    """Класс обработчик request"""

    data = {}

    def __init__(self, **kwargs):
        [setattr(self, attr, value) for attr, value in kwargs.items()]

    def as_dict(self) -> dict:
        """Формирует запрос в виде словаря"""

        return {attr: getattr(self, attr, None) for attr in self.__dict__}

    def is_valid(self) -> bool:
        """Проверка запроса на валидность"""

        return all([
            hasattr(self, 'time'),
            hasattr(self, 'action'),
            hasattr(self, 'username'),
        ])

    def __repr__(self):
        return str(self.as_dict())


class ResponseHandler:
    """
    Класс обработчик response.
    Чтобы сформировать готовый к отправке json ответ, необходимо применить метод make
    """

    data = {}

    def __init__(self, request: RequestHandler, data=None):

        self.data = {
            'action': getattr(request, 'action', None),
            'data': data,
        }

    def make(self, code: int = 200, info='OK') -> dict:
        """Формирование ответа c данными"""

        self.data.update({
            'time': int(datetime.now().timestamp()),
            'code': code,
            'info': info,
        })

        return self.data

    def make_400(self):
        """Формирование ответа c кодом 400"""
        return self.make(400, 'Wrong request format')

    def make_401(self):
        """Формирование ответа c кодом 401"""
        return self.make(401, 'Unauthorized')

    def make_403(self):
        """Формирование ответа c кодом 403"""
        return self.make(403, 'Access denied')

    def make_404(self):
        """Формирование ответа c кодом 404"""
        return self.make(404, 'Action is not supported')

    def make_500(self):
        """Формирование ответа c кодом 500"""
        return self.make(500, 'Internal server error')
