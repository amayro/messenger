import json
import select
import socket
import sys
import zlib
from argparse import ArgumentParser
from collections import deque, namedtuple
from typing import Deque

import yaml
from PyQt5.QtWidgets import QApplication

import routes
from chatapp.models import Chat
from db import ServerDB
from gui import ServerGui
from logger import get_logger
from observers import ClientNotifier
from protocol import ResponseHandler, RequestHandler
from settings import ENCODING_NAME, HOST, PORT, BUFFERSIZE

ConnectionUserTuple = namedtuple('ConnectionUserTuple', ['client', 'user_id'])
RequestClientTuple = namedtuple('RequestClientTuple', ['request', 'client'])


import faulthandler
faulthandler.enable()


class Server:
    def __init__(self, host, port, buffersize, encoding_name, workers=500,):
        self.host = host
        self.port = port
        self.buffersize = buffersize
        self.encoding_name = encoding_name
        self.workers = workers

        self.db = ServerDB(db_name='db_server.sqlite')
        self.sock = None
        self.logger = get_logger(type(self).__name__, encoding=self.encoding_name)

        self.connections_user: Deque[ConnectionUserTuple] = []
        self.requests: Deque[RequestClientTuple] = deque()
        self.connections = []

        self.responses = deque()

        self.notifier_client = ClientNotifier(self)

    def make_connection(self):
        """Создает сокет и устанавливает соединение"""

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((self.host, self.port))
        self.sock.listen(self.workers)
        self.sock.settimeout(0.2)
        self.logger.info(f'Server started with {self.host}:{self.port}')

    def accept_connection(self):
        """Принимает подключение от клиента"""

        try:
            client, address = self.sock.accept()

        except OSError:
            pass

        else:
            self.logger.info(f'Client detected {address}')
            self.connections.append(client)

    def receive_request(self, client):
        """Производит чтение данных от клиента и добавляет в список запросов"""

        try:
            b_request = client.recv(self.buffersize)
            self.requests.append(RequestClientTuple(client=client, request=b_request))

        except (ConnectionResetError, ConnectionAbortedError):
            self.logger.info(f'Client was disconnected {client}')
            self.connections.remove(client)
            for conn_user in self.connections_user:
                if conn_user.client == client:
                    self.connections_user.remove(conn_user)
                    break

            self.notifier_client.notify([conn.client for conn in self.connections_user])

    def send_response(self, client, response):
        """Отправление данных на клиент"""

        try:
            client.send(response)

        except ConnectionResetError:
            self.connections.remove(client)
            for conn_user in self.connections_user:
                if conn_user.client == client:
                    self.connections_user.remove(conn_user)
                    break

            self.notifier_client.notify([conn.client for conn in self.connections_user])

    def make_response(self, raw_request: bytes, client) -> tuple:
        """
        Обработка запроса по умолчанию.
        Возвращает список клиентов, кому нужно отправить сообщение, и сам ответ
        """

        request = json.loads(zlib.decompress(raw_request).decode(self.encoding_name))
        h_request = RequestHandler(**request)
        h_request.session = self.db.session
        self.logger.info(f'<--- {h_request}')

        send_to = []
        if h_request.is_valid():
            controller = routes.resolve(h_request.action)
            if controller:
                try:
                    d_response = controller(h_request)

                    if d_response.get('code') != 200:
                        self.logger.error(f'Request is not valid - {d_response}')
                    else:
                        self.logger.info(f'Function "{controller.__name__}" was called')

                    if h_request.action == 'user/login':
                        if d_response.get('code') == 200:
                            self.connections_user.append(
                                ConnectionUserTuple(client=client, user_id=d_response.get('data'))
                            )
                            self.notifier_client.notify([conn.client for conn in self.connections_user])

                    data = d_response.get('data')
                    if isinstance(data, dict):
                        chat_id = data.get('chat_id')
                        if chat_id:
                            chat_users_id = Chat.get_users(self.db.session, chat_id)
                            send_to = [con.client for con in self.connections_user if con.user_id in chat_users_id]

                except Exception as err:
                    self.logger.critical(err, exc_info=True)
                    d_response = ResponseHandler(h_request).make_500()
            else:
                self.logger.error(f'Action "{h_request.action}" does not exits')
                d_response = ResponseHandler(h_request).make_404()
        else:
            self.logger.error('Request is not valid')
            d_response = ResponseHandler(h_request).make_400()

        if not send_to:
            send_to = [client]

        self.logger.info(f'---> {d_response}')

        response = zlib.compress(json.dumps(d_response).encode(self.encoding_name))
        return send_to, response

    def start(self):
        """Запуск сервера"""

        self.make_connection()

        while True:
            self.accept_connection()

            try:
                ready_to_read, ready_to_write, _ = select.select(self.connections, self.connections, [], 0)
            except Exception:
                pass
            else:
                for r_client in ready_to_read:
                    self.receive_request(r_client)

                if self.requests:
                    request_cl: RequestClientTuple = self.requests.popleft()
                    send_to, response = self.make_response(
                        request_cl.request,
                        request_cl.client,
                    )

                    for client in send_to:
                        self.send_response(client, response)

    def close(self):
        """Остановка сервера"""

        if hasattr(self, 'sock'):
            self.sock.close()
            self.logger.info('Server closed')


if __name__ == '__main__':
    host = HOST
    port = PORT
    encoding_name = ENCODING_NAME
    buffersize = BUFFERSIZE

    parser = ArgumentParser()
    parser.add_argument('-c', '--config', type=str, help='Sets run configuration')

    args = parser.parse_args()

    if args.config:
        with open(args.config) as file:
            config = yaml.load(file, Loader=yaml.Loader)
            host = config.get('host') or HOST
            port = config.get('port') or PORT
            encoding_name = config.get('encoding_name') or ENCODING_NAME
            buffersize = config.get('buffersize') or BUFFERSIZE

    server = Server(host=host,
                    port=port,
                    buffersize=buffersize,
                    encoding_name=encoding_name)

    app = QApplication([])
    gui = ServerGui(server)
    gui.show()

    sys.exit(app.exec_())
