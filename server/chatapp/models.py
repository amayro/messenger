from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from db import Base, SessionContext
from models import Core


class Chat(Core, Base):
    """Модель для чата"""

    title = Column(String(32), nullable=False)

    message = relationship('Message', backref='chat')

    def __init__(self, title):
        self.title = title

    def __repr__(self):
        return f"<Chat(id={self.id}, title={self.title})>"

    @classmethod
    def get_by_title(cls, session, title):
        """
        Получить объект класса из базы.
        Если объекта с title в базе не существует, возвращается None.
        """

        obj = session.query(cls).filter(cls.title == title).first()
        return obj

    @classmethod
    def get_users(cls, session, chat_id):
        """Получить id всех пользователей чата"""

        objs = session.query(cls).filter(cls.id == chat_id).first()
        print('get_users objs', objs.user)
        return [user.id for user in objs.user]

    def add_member(self, session, user):
        """Добавляет участника в чат"""

        self.update(session, user=self.user.append(user))

    @classmethod
    def create(cls, session, *args, **kwargs):
        """Добавить в базу"""

        with SessionContext(session) as session:
            [kwargs.pop(kw) for kw in list(kwargs.keys()) if not hasattr(cls, kw)]
            obj = cls(*args, **kwargs)
            session.add(obj)
            session.commit()
        return obj
