import os
from pathlib import PurePath

from .controllers import (
    get_or_create_private_chat,
    update_chat,
    delete_chat,
    get_chat_msg
)

routes = [
    {'action': 'get_private', 'controller': get_or_create_private_chat},
    {'action': 'update', 'controller': update_chat},
    {'action': 'delete', 'controller': delete_chat},

    {'action': 'get_chat_msg', 'controller': get_chat_msg},
]

app_name = PurePath(os.path.dirname(__file__)).parts[-1].rstrip('app')
app_routes = [
    {'action': f"{app_name}/{route.get('action')}",
     'controller': route.get('controller')}
    for route in routes
]