from decorators import need_attributes
from messageapp.models import Message

from protocol import ResponseHandler
from userapp.models import User
from .models import Chat


def _create_chat(request, user: User, title) -> Chat:
    """Создает контакт для пользователя"""

    new_chat = Chat.create(request.session, title=title)
    new_chat.add_member(request.session, user)

    return new_chat


def update_chat(request):
    """Обновляет контакт"""

    pass


@need_attributes(['username', 'chat_name'])
def delete_chat(request):
    """Удаляет контакт"""

    pass


@need_attributes(['username', 'contact_name'])
def get_or_create_private_chat(request):
    """Получение одного контакта для пользователя"""

    user = User.get_by_username(request.session, request.username)
    user_contact = User.get_by_username(request.session, request.contact_name)

    chat_title = f'_{user.id}_{user_contact.id}' if user.id < user_contact.id else f'_{user_contact.id}_{user.id}'
    chat = Chat.get_by_title(request.session, title=chat_title)
    if not chat:
        chat = _create_chat(request, user, chat_title)

    if chat not in user_contact.chat:
        chat.add_member(request.session, user_contact)

    return ResponseHandler(request, data=chat.id).make(info=f'GET chat success {str(chat)}')


@need_attributes(['username', 'chat_id'])
def get_chat_msg(request):
    """Получение сообщений чата"""

    messages = Message.get_all_for_chat(request.session, request.chat_id)
    data = [{
        'username': msg.owner.username,
        'time': int(msg.created_dt.timestamp()),
        'text': msg.text
    } for msg in messages]

    return ResponseHandler(request, data=data).make(info=f'GET messages success')
