import os
from pathlib import PurePath

from .controllers import (
    get_date_now
)

routes = [
    {'action': 'now', 'controller': get_date_now},
]

app_name = PurePath(os.path.dirname(__file__)).parts[-1].rstrip('app')
app_routes = [
    {'action': f"{app_name}/{route.get('action')}",
     'controller': route.get('controller')}
    for route in routes
]
