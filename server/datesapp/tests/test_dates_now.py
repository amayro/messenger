from datetime import datetime

from datesapp.controllers import get_date_now
from protocol import RequestHandler


def test_get_date_now():
    """Тест получения даты"""

    date = datetime.now()
    success_date = date.strftime('%Y.%m.%d')

    request = RequestHandler(**{
        'time': int(datetime.now().timestamp()),
        'action': 'now',
        'username': 'someuser'
    })

    response = get_date_now(request)

    assert response.get('data') == success_date
