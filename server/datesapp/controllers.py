from datetime import datetime

from decorators import logged, login_required
from protocol import ResponseHandler


@login_required
@logged
def get_date_now(request):
    """Формирование и отправка даты"""

    data = datetime.now().strftime('%Y.%m.%d')

    return ResponseHandler(request, data).make()
