BUFFERSIZE = 1024
ENCODING_NAME = 'utf-8'
HOST = 'localhost'
PORT = 8000

INSTALLED_MODULES = [
    'datesapp',
    'echoapp',
    'userapp',
    'chatapp',
    'contactapp',
    'messageapp',
]
